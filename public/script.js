window.addEventListener('DOMContentLoaded', function()
{
  var select = document.querySelector('#main'),
  hide = document.querySelectorAll('.hide');
  function change()
  {
    [].forEach.call(hide, function(el)
    {
           var add = el.classList.contains(select.value) ? "add" : "remove"
           el.classList[add]('show');
    });
  }
  select.addEventListener('change', change);
  change()
});

function getValueOfType()
{
  var select = document.getElementById("main");
  var value = select.value;
  return value;
}

function getValueOfOption()
{
  var radios = document.getElementsByName('option');
  for (var i = 0, length = radios.length; i < length; i++)
  {
    if (radios[i].checked)
    {
      return radios[i].value;
    }
  }
}

function isChecked()
{
  var checkbox = document.getElementById("checkbox");
  if (checkbox.checked)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

function calculate()
{
  let valueOfGoods = document.getElementsByName("valueOfGoods");
  var val1 = parseInt(valueOfGoods[0].value);
  var regax = /[0-9]/;
  var provFr = regax.test(val1);

  var val2 = getValueOfType();

  var val3 = getValueOfOption();

  var val4 = isChecked();

  let forReturn = document.getElementById("result");

  if ((provFr == true) && (val1 > 0))
  {
    if (val2 == 1)
    {
      forReturn.innerHTML = 22000 * val1;
    }
    else if (val2 == 2)
    {
      if (val3 == 1)
      {
        forReturn.innerHTML = 28000 * val1;
      }
      else if (val3 == 2)
      {
        forReturn.innerHTML = 28500 * val1;
      }
      else
      {
        forReturn.innerHTML = 29000 * val1;
      }
    }
    else
    {
      if (val4 == 0)
      {
        forReturn.innerHTML = 32000 * val1;
      }
      else
      {
        forReturn.innerHTML = 36000 * val1;
      }
    }
  }
  else
  {
    forReturn.innerHTML = "";
  }
  
  return false;
}

window.addEventListener('DOMContentLoaded', function (event)
{
  console.log("DOM fully loaded and parsed");
  let t = document.getElementById("main");
  t.addEventListener('change', calculate);
  let o = document.getElementsByName('option');
  o.forEach(function(radio)
  {
    radio.addEventListener('click', calculate)
  })
  let c = document.getElementById("checkbox");
  c.addEventListener('click', calculate);
  let n = document.getElementById("valueOfGoods");
  n.addEventListener('input', calculate);
});